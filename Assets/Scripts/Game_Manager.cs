﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class Game_Manager : MonoBehaviour {

    //Player Resources
    public int money = 10;
    public int winGoal = 20;
    public GameObject Pet;

    //Pet Variables
    private int Happiness;
    private int Hunger;
    private int Experience;
    private int HappyMax;
    private int HungerMax;

    //Time Limits
    bool storeOpen = false;
    float workTime = 5;
    bool isWorking = false;
    bool full = false;
    float fullTime = 3;
    bool HungerDecaying = true;
    bool HappyDecaying = false;
    float HungerDecay = 11;
    float HappyDecay = 11;
    int WorkLimit = 0;
    float WorkTimeLimit = 30;

    //Gui Elements
    public Text MoneyAMT;
    public Text ShopMoney;
    public Text GameOver;
    public Image WorkScene;
    public Image ShopScene;

    //Debug Texts
    public Text HungerTimer;
    public Text HappyTimer;
    public Text WorkTimer;

	// Use this for initialization
	void Start () {
    
	}
	
	// Update is called once per frame
	void Update () {

        //Displays amount of money
        MoneyAMT.text = "Money: $" + money;
        ShopMoney.text = "Money: $" + money;

        //Allows modification of pet traits
        Happiness = Pet.GetComponent<Pet_Manager>().Happiness;
        Hunger = Pet.GetComponent<Pet_Manager>().Hunger;
        Experience = Pet.GetComponent<Pet_Manager>().expLevel;
        HappyMax = Pet.GetComponent<Pet_Manager>().HappyMax;
        HungerMax = Pet.GetComponent<Pet_Manager>().HungerMax;

        //resets happiness and hunger to zero if it ever goes below zero
        if (Happiness < 0)
        {
            Pet.GetComponent<Pet_Manager>().Happiness = 0;
        }

        if (Hunger < 0)
        {
            Pet.GetComponent<Pet_Manager>().Hunger = 0;
        }

        //DEBUG CONTROLS
        /*if (Input.GetButtonDown("Add"))
        {
            Pet.GetComponent<Pet_Manager>().Hunger += 1;
        }

        if (Input.GetButtonDown("Remove"))
        {
            Pet.GetComponent<Pet_Manager>().Hunger -= 1;
        }*/

        //SHOPPING

<<<<<<< HEAD
        if (Input.GetButtonDown("Start") && isWorking == false)
=======
        if(Input.GetButtonDown("Start") && isWorking == false)
>>>>>>> origin/master
        {
            ShopScene.gameObject.SetActive(true);
            storeOpen = true;
            Debug.Log("Opened the store");
        }

        if (Input.GetButtonDown("A") && storeOpen == true && isWorking == false)
            {
                ShopScene.gameObject.SetActive(false);
                storeOpen = false;
                money -= 3;
<<<<<<< HEAD
                Pet.GetComponent<Pet_Manager>().Hunger += 2;
=======
                Pet.GetComponent<Pet_Manager>().Hunger += 1;
>>>>>>> origin/master
                Pet.GetComponent<Pet_Manager>().Happiness -= 1;
                Debug.Log("Bought some bread");
            }

            if (Input.GetButtonDown("X") && storeOpen == true && isWorking == false)
            {
                ShopScene.gameObject.SetActive(false);
                storeOpen = false;
                money -= 5;
                Pet.GetComponent<Pet_Manager>().Hunger += 3;
<<<<<<< HEAD
                Debug.Log("Bought some fruit");
=======
                Debug.Log("Bought some meat");
>>>>>>> origin/master
            }

            if (Input.GetButtonDown("Y") && storeOpen == true && isWorking == false)
            {
                ShopScene.gameObject.SetActive(false);
                storeOpen = false;
                money -= 10;
                Pet.GetComponent<Pet_Manager>().Hunger += 5;
                Pet.GetComponent<Pet_Manager>().Happiness += 3;
                Debug.Log("Bought some cake");
            }

            if (Input.GetButtonDown("B"))
            {
                ShopScene.gameObject.SetActive(false);
                storeOpen = false;
            }

        //WORKING

        //brings up work image
        if (Input.GetButtonDown("Exit") && storeOpen == false)
        {
            WorkScene.gameObject.SetActive(true);
            isWorking = true;
            WorkLimit += 1;
        }

        //if is working is true, counts down from 5
        if (isWorking == true)
        {
            workTime -= Time.deltaTime;
            Debug.Log(workTime);
        }

        //displays work timer
        WorkTimer.text = " Work Limit: " + WorkTimeLimit.ToString("#");

        //if player presses work 3 times within 30 seconds, happiness goes down by 8
        if(WorkLimit > 0)
        {
            WorkTimeLimit -= Time.deltaTime;
            if(WorkTimeLimit < 0)
            {
                WorkTimeLimit = 30;
                WorkLimit = 0;
            }

            if(WorkLimit >= 3 && WorkTimeLimit > 0)
            {
                Pet.GetComponent<Pet_Manager>().Happiness -= 8;
                WorkLimit = 0;
                WorkTimeLimit = 30;
            }
        }

        //once work is over, worktime resets, image goes away, and pet gains 1 happiness and 1 exp
        if (workTime < 0)
        {
            WorkScene.gameObject.SetActive(false);
            isWorking = false;
            workTime = 5;
            //Pet.GetComponent<Pet_Manager>().Happiness += 1;
            Pet.GetComponent<Pet_Manager>().expLevel += 1;
            money += 3;
        }

        //every 10 seconds, hunger decays by one. Timer resets after subtracting.
        //if exp is at 2, cooldown increases to 15 seconds
        //if exp is at 5, cooldown increases to 20 seconds
        //if exp is at 10, cooldown increases to 25 seconds

        if (HungerDecaying == true)
        {
            HungerDecay -= Time.deltaTime;
            HungerTimer.text = " Nom: " + HungerDecay.ToString("#");
        }

        if(HungerDecay < 0 && HungerDecaying == true && Experience < 2)
        {
            Pet.GetComponent<Pet_Manager>().Hunger -= 1;
            HungerDecay = 11;
        }else if(HungerDecay < 0 && HungerDecaying == true && Experience >= 2 && Experience <= 4)
        {
            Pet.GetComponent<Pet_Manager>().Hunger -= 1;
            HungerDecay = 16;
        }
        else if (HungerDecay < 0 && HungerDecaying == true && Experience >= 5 && Experience <= 9)
        {
            Pet.GetComponent<Pet_Manager>().Hunger -= 1;
            HungerDecay = 21;
        }
        else if (HungerDecay < 0 && HungerDecaying == true && Experience >= 10)
        {
            Pet.GetComponent<Pet_Manager>().Hunger -= 1;
            HungerDecay = 26;
        }

        //if hunger reaches zero, happiness decays at a rate of 1 per 10 seconds
        //if exp is at 2, cooldown increases to 15 seconds
        //if exp is at 5, cooldown increases to 20 seconds
        //if exp is at 10, cooldown increases to 25 seconds

        if (Hunger == 0)
        {
            HungerDecaying = false;
            if (HungerDecay < 11) //resets timer
            {
                HungerDecay = 11;
            }
            HappyDecaying = true;
        }

        if (Hunger > 0 && HappyDecaying == true)
        {
            HappyDecaying = false;
            if (HappyDecay < 11)//resets timer
            {
                HappyDecay = 11;
            }
            HungerDecaying = true;
        }

        if (HappyDecaying == true)
        {
            HappyDecay -= Time.deltaTime;
            HappyTimer.text = " Sad: " + HappyDecay.ToString("#");
        }

        if(HappyDecay < 0 && HappyDecaying == true)
        {
            Pet.GetComponent<Pet_Manager>().Happiness -= 1;
            HappyDecay = 11;
        }
        else if (HappyDecay < 0 && HappyDecaying == true && Experience >= 2 && Experience <= 4)
        {
            Pet.GetComponent<Pet_Manager>().Hunger -= 1;
            HappyDecay = 16;
        }
        else if (HappyDecay < 0 && HappyDecaying == true && Experience >= 5 && Experience <= 9)
        {
            Pet.GetComponent<Pet_Manager>().Hunger -= 1;
            HappyDecay = 21;
        }
        else if (HappyDecay < 0 && HappyDecaying == true && Experience >= 10)
        {
            Pet.GetComponent<Pet_Manager>().Hunger -= 1;
            HappyDecay = 26;
        }

        //if happiness reaches zero, hunger decays by 1 every 5 seconds
        if (Happiness == 0)
        {
            //dsfargeg
        }

        //if hunger reaches its maximum, happiness increases by 1 every 3 seconds

        if (Hunger >= HungerMax)
        {
            full = true;
        }
        else
        {
            full = false;
            if(fullTime < 3)
            {
                fullTime = 3;
            }
        }

        if (full == true)
        {
            fullTime -= Time.deltaTime;
        }

        if(fullTime < 0)
        {
            Pet.GetComponent<Pet_Manager>().Happiness += 1;
            fullTime = 3;
        }

        //death state
        if(Happiness == 0 && Hunger == 0)
        {
            GameOver.text = " Game Over!";
            HappyDecaying = false;
            HungerDecaying = false;
            HappyDecay = 11;
        }

        //For exiting a build quickly
        if (Input.GetButtonDown("LBumper"))
        {
            Application.Quit();
        }

	}
}