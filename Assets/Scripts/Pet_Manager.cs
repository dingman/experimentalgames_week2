﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class Pet_Manager : MonoBehaviour {

    //Pet Traits
    public int Happiness = 5;
    public int Hunger = 5;
    public int expLevel = 0;

    public int HappyMax = 10;
    public int HungerMax = 10;

    //GUI
    public Text HappyText;
    public Text HungerText;
    public Text ExpText;

    //Animator
    private Animator anim;

    //Animation States
    const int STATE_IDLE = 0;
    const int STATE_HAPPY = 1;
    const int STATE_SAD = 2;
    const int STATE_DEAD = 3;

    int _currentAnimationState = STATE_IDLE;

	// Use this for initialization
	void Start () {

        anim = GetComponent<Animator>();
        changeState(STATE_IDLE);

	}
	
	// Update is called once per frame
	void Update () {

        HappyText.text = " Happiness: " + Happiness;
        HungerText.text = " Hunger Level: " + Hunger;
        ExpText.text = " Experience: " + expLevel;

<<<<<<< HEAD
        if(Happiness >= 0 && Happiness <= 2)
=======
        if(Happiness >= 1 && Happiness <= 2)
>>>>>>> origin/master
        {
            changeState(STATE_SAD);
        }

        if(Happiness > 5)
        {
            changeState(STATE_HAPPY);
        }

        if(Happiness == 0 && Hunger == 0)
        {
            changeState(STATE_DEAD);
        }

        if(Happiness >= 3 && Happiness <= 5)
        {
            changeState(STATE_IDLE);
        }

	}

    //Change pet state
    void changeState(int state)
    {
        if(_currentAnimationState == state)
        {
            return;
        }

        switch (state)
        {
            case STATE_IDLE:
                anim.SetInteger("State", STATE_IDLE);
                break;
            case STATE_HAPPY:
                anim.SetInteger("State", STATE_HAPPY);
                break;
            case STATE_SAD:
                anim.SetInteger("State", STATE_SAD);
                break;
            case STATE_DEAD:
                anim.SetInteger("State", STATE_DEAD);
                break;
        }

        _currentAnimationState = state;
    }
}
